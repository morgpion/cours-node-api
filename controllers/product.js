
const Product = require('../models/Product');

exports.createProduct = (req, res) => {
    const product = req.body
    const newProduct = new Product({
        ...product
    })
    console.log(newProduct);
    newProduct.save().then(result => {
        res.status(201).json({
            message: "Produit ajouté"
        })
    }).catch(err => {
        res.status(400).json({message: "erreur "+err});
    })
}

exports.getProducts = ((req, res) => {
    Product.find().then(products => {
        res.status(200).json(products);
    }).catch((err) => {
        res.status(400).json(err);
    })
});

exports.getProductByName = ((req, res) => {
    Product.findOne({
        name: req.params.name
    }).then((product) => {
        res.status(200).json(product);
    }).catch((err) => {
        res.status(400).json(err);
    })
});

exports.getProductById = ((req, res) => {
    Product.findOne({
        _id: req.params.id
    }).then((product) => {
        res.status(200).json(product);
    }).catch((err) => {
        res.status(400).json(err);
    })
});

//  méthode updateProduct : récupère la requete, 
//  stocke le contenu de body dans une variable updatedProduct selon le schéma du model Product
exports.updateProduct = ((req, res) => {
    console.log(req.body);
    const updatedProduct = new Product({
          ...req.body
    });
    console.log(updatedProduct);
    //  model.findByIdAndUpdate : getById d'après le param (id) de l'url, 
    //  appeler updateProduct et afficher la réponse
    // /!\  Produit n'est pas envoyé à la bdd, ou la bdd ne le reçoit pas : /!\
    Product.findByIdAndUpdate(req.params.id, {new: true}, {updatedProduct}).then((result) => {
        //  Affiche le message alors que le prdt n'a pas été mis à jour :
        res.status(201).json({message: "Le produit a été mis à jour"});
    }).catch((err) => {
        res.status(400).json(err);
    });
});

exports.deleteProduct = ((req, res) => {
    Product.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({message: "Le produit a été supprimé"});
    }).catch((err) => {
        res.status(400).json(err);
    })
});


