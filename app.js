﻿const express = require('express');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
const productsController = require('./controllers/product');
const cors = require('cors');

const app = express();  
mongoose.connect('mongodb+srv://user1:bewebNimes30@cluster0-8gsay.gcp.mongodb.net/api', { 
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Connexion mongodb ok");
}).catch(err => {
    console.log(err);
})

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());
app.use(cors());

//  mettre les routes dans un fichier à part
app.get('/products', productsController.getProducts);
app.delete('/products/:id', productsController.deleteProduct);
app.get('/productsName/:name', productsController.getProductByName);
app.get('/products/:id', productsController.getProductById);
app.put('/products/:id', productsController.updateProduct);
app.post('/products', productsController.createProduct);



module.exports = app;


