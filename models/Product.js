const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: true}
},
{ 
    timestamps: { 
        createdAt: 'created_at' 
    } 
});

/*
const NoteSchema = mongoose.Schema({
    title: String,
    content: String
},  {
    timestamps: true
});
*/

module.exports = mongoose.model('Product', ProductSchema)
